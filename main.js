// Global Variables
var
    game = new Phaser.Game(1024, 768, Phaser.CANVAS, ''),
    Main = function () {},
    gameOptions = {
        playSound: true,
        playMusic: true,
        gameStage: 0,
        highScore: 0,
    },
    musicPlayer,
    PLAYER_DATA = null;

Main.prototype = {
    init: function(){
        // when user navigate to differ browser then game will be pause
        game.stage.disableVisibilityChange = false;
        //show all content of our canvas
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.setShowAll();
        // min width/height of game
        game.scale.minWidth = 460;
        game.scale.minHeight = 300;
        game.scale.maxWidth = 1024;
        game.scale.maxHeight = 768;
        //centerlize the game
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        //force the portrait mode of game
        game.scale.forceLandscape = true;
        //game.stage.forcePortrait = true;
        //console.log(game.scale);
        game.scale.updateLayout(true);   
    },
    preload: function () {
        game.load.image('loading', 'assets/images/loading.png');
        game.load.image('brand', 'assets/images/game-logo.png');
        game.load.script('splash', 'states/boot.js');
    },

    create: function () {
        if (typeof(Storage) !== "undefined") {
            // Code for localStorage/sessionStorage.
            if (!PLAYER_DATA) {
                // retrieve from local storage (to view in Chrome, Ctrl+Shift+J -> Resources -> Local Storage)
                var phaser_game_player_data = window.localStorage.getItem('phaser_game_player_data');

                // error checking, localstorage might not exist yet at first time start up
                try {
                        PLAYER_DATA = JSON.parse(phaser_game_player_data);
                } catch(e){
                        PLAYER_DATA = []; //error in the above string(in this case,yes)!
                }
                // error checking just to be sure, if localstorage contains something else then a JSON array (hackers?)
                if (Object.prototype.toString.call( PLAYER_DATA ) !== '[object Array]' ) {
                        PLAYER_DATA = [];
                }
            }
            console.log(PLAYER_DATA);
        } else {
            // Sorry! No Web Storage support..
        }
        game.state.add('boot', boot);
        game.state.start('boot');
    }

};

game.state.add('Main', Main);
game.state.start('Main');